ruleset manage_sensors_test {
  meta {
    shares __testing, test_manager
    use module io.picolabs.wrangler alias Wrangler
    use module manage_sensors
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }
      ] , "events":
      [
        {"domain": "manage_sensors_test", "type": "start"}
      ]
    }

    s1_pn = "test_sensor1 profile name"
    s2_pn = "test_sensor2 profile name"

  }

  rule test_start {
    select when manage_sensors_test start
    pre {
      a = klog("Starting test suite...")
      starting_children = manage_sensors:sensors()
    }
    send_directive("test_start", {"message": "starting manage_sensors test"})
    always {
      // test creating a sensor
      log info "Creating new children...";
      ent:starting_children := starting_children;
      ent:test_active := true;
      ent:s1_created := false;
      ent:s2_created := false;
      ent:s1_deleted := false;
      ent:s2_deleted := false;
      raise sensor event "new_sensor"
        attributes {"name": "test_sensor1", "profile_name": s1_pn};
      raise sensor event "new_sensor"
        attributes {"name": "test_sensor2", "profile_name": s2_pn};
    }
  }

  rule step1a {
    select when wrangler child_initialized where ent:test_active && (event:attr("name") == "test_sensor1")
    pre {
      a = ent:s2_created.klog("s2")
    }
    if ent:s2_created then
      noop()
    fired {
      raise manage_sensors_test event "created";
      ent:s1_created := true;
    } else {
      ent:s1_created := true;
    }
  }

  rule step1b {
    select when wrangler child_initialized where ent:test_active && (event:attr("name") == "test_sensor2")
    pre {
      a = ent:s1_created.klog("s1")
    }
    if ent:s1_created then
      noop()
    fired {
      raise manage_sensors_test event "created";
      ent:s2_created := true;
    } else {
      ent:s2_created := true;
    }
  }

  rule step2 {
    select when manage_sensors_test created
    pre {
      a = klog("Testing child creation...")
      sensors = manage_sensors:sensors()
      s1_exists = sensors >< "test_sensor1"
      s2_exists = sensors >< "test_sensor2"
    }
    if (s1_exists && s2_exists) then
      send_directive("test_created", {"message": "test_sensor1 and test_sensor2 created successfully"})
    fired {
      // ensure sensor profiles were setup correctly
      log info "Children created successfully...";
      raise manage_sensors_test event "profiles"
        attributes {"s1_eci": sensors{"test_sensor1"}{"eci"}, "s2_eci": sensors{"test_sensor2"}{"eci"}}
    } else {
      log error "sensors were not created or stored properly";
      ent:test_active := false;
      raise manage_sensors_test event "after";
    }
  }

  rule step3 {
    select when manage_sensors_test profiles
    pre {
      a = klog("Testing profile setup...")
      s1_profile = Wrangler:skyQuery(event:attr("s1_eci"), "sensor_profile", "retrieve_profile")
      s1_passes = (s1_profile{"name"} == s1_pn) 
          && (s1_profile{"threshold"} == manage_sensors:default_threshold())
      s2_profile = Wrangler:skyQuery(event:attr("s2_eci"), "sensor_profile", "retrieve_profile")
      s2_passes = (s2_profile{"name"} == s2_pn) 
          && (s2_profile{"threshold"} == manage_sensors:default_threshold())
    }
    if s1_passes && s2_passes then every {
      // ensure sensors respond appropriately to new temperature events
      send_directive("test_profile", {"message": "profiles were initialized correctly"});
      event:send({
        "eci": event:attr("s1_eci"),
        "eid": "test_temperatures",
        "domain": "wovyn_test",
        "type": "raise_violation",
        "attrs": {"temperature": 70}
      });
      event:send({
        "eci": event:attr("s1_eci"),
        "eid": "test_temperatures",
        "domain": "wovyn_test",
        "type": "raise_violation",
        "attrs": {"temperature": 79}
      });
      event:send({
        "eci": event:attr("s2_eci"),
        "eid": "test_temperatures",
        "domain": "wovyn_test",
        "type": "raise_violation",
        "attrs": {"temperature": 72}
      });
      event:send({
        "eci": event:attr("s2_eci"),
        "eid": "test_temperatures",
        "domain": "wovyn_test",
        "type": "raise_violation",
        "attrs": {"temperature": 80}
      });
    }
    fired {
      // schedule next test step; wait some time for the events to propagate through the child picos
      log info "profiles set correctly...";
      schedule manage_sensors_test event "temperatures" at time:add(time:now(), {"seconds": 0.2})
        attributes {"s1_eci": event:attr("s1_eci"), "s2_eci": event:attr("s2_eci")}
    } else {
      log error "sensor profiles were not updated properly";
      ent:test_active := false;
      raise manage_sensors_test event "after";
    }
  }

  rule step4 {
    select when manage_sensors_test temperatures
    // ensure temperatures were added correctly, and that the manager can access them
    pre {
      a = klog("Testing recording temperatures...")
      s1_t  = Wrangler:skyQuery(event:attr("s1_eci"), "temperature_store", "temperatures")
      s1_tv = Wrangler:skyQuery(event:attr("s1_eci"), "temperature_store", "threshold_violations")
      s2_t  = Wrangler:skyQuery(event:attr("s2_eci"), "temperature_store", "temperatures")
      s2_tv = Wrangler:skyQuery(event:attr("s2_eci"), "temperature_store", "threshold_violations")
      sm = manage_sensors:all_sensor_records()
      tests_passed = (s1_t[0]{"temperature"} == 70)
          && (s1_t[1]{"temperature"} == 79)
          && (s1_tv[0]{"temperature"} == 79)
          && (s2_t[0]{"temperature"} == 72)
          && (s2_t[1]{"temperature"} == 80)
          && (s2_tv[0]{"temperature"} == 80)
          && (sm{"test_sensor1"} == s1_t)
          && (sm{"test_sensor2"} == s2_t)
    }
    if tests_passed then
      send_directive("test_temperatures", {"message": "temperatures handled correctly"})
    fired {
      // setup for next test: adding duplicate sensor
      log info "Temperatures recorded correctly...";
      raise sensor event "new_sensor"
        attributes {"name": "test_sensor1", "profile_name": s1_pn};
      schedule manage_sensors_test event "duplicate" at time:add(time:now(), {"seconds": 0.1})
    } else {
      log error "temperatures were not handled correctly by sensors";
      ent:test_active := false;
      raise manage_sensors_test event "after";
    }
  }

  rule step5 {
    select when manage_sensors_test duplicate
    pre {
      a = klog("Testing duplicate sensor creation...")
      sensors = manage_sensors:sensors()
      children = Wrangler:children()
    }
    if (sensors.length() == ent:starting_children.length() + 2 
        && children.length() == ent:starting_children.length() + 2) then
      send_directive("test_duplicate", {"message": "duplicate sensor was not added"})
    fired {
      // test deleting children
      log info "Success: duplicate child was not added...";
      log info "Testing child deletion...";
      raise manage_sensors_test event "after";
    } else {
      log error "duplicate sensor should not have been added";
      ent:test_active := false;
      raise manage_sensors_test event "after";
    }
  }

  rule step6a {
    select when information child_deleted where ent:test_active && (event:attr("name") == "test_sensor1")
    pre {
      a = ent:s2_deleted.klog("s2")
    }
    if ent:s2_deleted then
      noop()
    fired {
      raise manage_sensors_test event "delete";
      ent:s1_deleted := true;
    } else {
      ent:s1_deleted := true;
    }
  }

  rule step6b {
    select when information child_deleted where ent:test_active && (event:attr("name") == "test_sensor2")
    pre {
      a = ent:s1_deleted.klog("s1")
    }
    if ent:s1_deleted then
      noop()
    fired {
      raise manage_sensors_test event "delete";
      ent:s2_deleted := true;
    } else {
      ent:s2_deleted := true;
    }
  }

  rule step7 {
    select when manage_sensors_test delete
    pre {
      sensors = manage_sensors:sensors().klog("manager sensors")
      //children = Wrangler:children().klog("children") // NOTE: this one still returns the deleted children; the engine actually hasn't cleaned them up yet, even though the wrangler reported a deletion; is this a bug in the engine?
      b = ent:starting_children.klog("starting children")
    }
    if (sensor.length() == ent:starting_children.length()
        // && children == ent:starting_children
        ) then
      send_directive("test_delete", {"message": "sensors deleted successfully"});
    fired {
      // done
      log info "Sensors deleted successfully. End of tests...";
      ent:test_active := false;
      ent:starting_children := [];
    }
    else {
      log error "sensors were not deleted";
      ent:test_active := false;
    }
  }

  rule after_tests {
    select when manage_sensors_test after
    send_directive("test_after", {"message": "cleaning up sensors..."})
    always {
      raise sensor event "unneeded_sensor"
        attributes {"name": "test_sensor1"};
      raise sensor event "unneeded_sensor"
        attributes {"name": "test_sensor2"};
    }
  }
}
